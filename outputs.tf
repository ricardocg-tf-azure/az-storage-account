output "id" {
    value           = azurerm_storage_account.example.id
    description     = "ID for the SA"
}

output "location" {
    value           = azurerm_storage_account.example.primary_location
    description     = "Location for the SA"
}

output "primary_blob_endpoint" {
    value           = azurerm_storage_account.example.primary_blob_endpoint
    description     = ""
}

output "name" {
    value           = azurerm_storage_account.example.name
    description     = ""
}
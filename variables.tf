variable "name" {
    description         = "Name for the account"
}

variable "rg" {
    description         = "Name for the resource group"
}

variable "location" {
    description         = "Location for the storage account "
}

variable "tier" {
    description         = "Type of tier for the account"
    default             = "Standard"
}

variable "replication" {
    description         = "Type of replication "
    default             = "GRS"
}

variable "env" {
    description         = "Environment where the account runs, used for tags control"
    default             = "test"
}
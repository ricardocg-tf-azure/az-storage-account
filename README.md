# Storage Account Module for AZURE

- 

# Inputs 

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| VPC | The ID of the VPC | String | - |:yes:|

# Outputs 

| Name | Description |
|------|-------------|
| ID | ID of the group created |
| Location | Location of the group created |

# Usage

